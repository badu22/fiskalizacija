<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */
$args_docs = array(
    'post_type' => 'dokument',
    'posts_per_page' => -1,
);

$args_pitanja = array(
    'post_type' => 'pitanje',
    'posts_per_page' => -1,
);

$args_baneri = array(
    'post_type' => 'baner',
    'posts_per_page' => -1,
);

$args_uputstva = array(
    'post_type' => 'uputstvo',
    'posts_per_page' => 6,
    'order' => 'asc'
);

$args_kontakt = array(
    'post_type' => 'kontakt',
    'posts_per_page' => 1,
    'order' => 'asc'
);

$context = Timber::context();
$context['posts'] = new Timber\PostQuery();
$context['dokumenti'] = Timber::get_posts($args_docs);
$context['pitanja'] = Timber::get_posts($args_pitanja);
$context['baneri'] = Timber::get_posts($args_baneri);
$context['uputstva'] = Timber::get_posts($args_uputstva);
$context['kontakt'] = Timber::get_posts($args_kontakt);

$context['is_front_page'] = 'true';

// print_r();

$templates = array( 'index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'front-page.twig', 'home.twig' );
}
Timber::render( $templates, $context );