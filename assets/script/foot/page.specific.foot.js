// alert('hello');
// console.log('this is page specific in FOOTER');

/*
?>

<script>
    jQuery(document).ready(function ($) {
        var deviceAgent = navigator.userAgent.toLowerCase();
        if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
            $("html").addClass("ios");
            $("html").addClass("mobile");
        }
        if (navigator.userAgent.search("MSIE") >= 0) {
            $("html").addClass("ie");
        }
        else if (navigator.userAgent.search("Chrome") >= 0) {
            $("html").addClass("chrome");
        }
        else if (navigator.userAgent.search("Firefox") >= 0) {
            $("html").addClass("firefox");
        }
        else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
            $("html").addClass("safari");
        }
        else if (navigator.userAgent.search("Opera") >= 0) {
            $("html").addClass("opera");
        }
    });
</script>

<?php
*/

// Univerzalne varijable
var viewWidth = document.querySelector('body').offsetWidth;

// Navigation Smooth Scroll
if(document.querySelector('.home')) {
    ;(function($){
        $('.navbar__item').on('click', function(event) {
            if (this.hash !== '') {
                event.preventDefault();
        
                const hash = this.hash;
        
                $('html, body').animate(
                    {
                        scrollTop: $(hash).offset().top - 70
                    },
                    800
                );
            }
        });
        
        if(!!window.IntersectionObserver){
            let prefix = "#";
            let observer = new IntersectionObserver((entries, observer) => { 
                entries.forEach(entry => {
                    if(entry.isIntersecting){
                        let anchor = prefix + entry.target.id
                        $('.navbar__menu .navbar__item').removeClass('current');
                        $('[href=' + anchor + ']').addClass('current');
                    }
                });
            }, {rootMargin: "0px 0px -100px 0px", threshold: 0.5});
            document.querySelectorAll('#pocetna, #vesti, #potrebno, #dokumenti, #pitanja').forEach(elem => { observer.observe(elem) });
        }
    })(jQuery);

    // Swiper Vesti
    const swiperVesti = new Swiper('.swiperVesti', {
        // Optional parameters
        direction: 'horizontal',
        // slidesPerView: 4,
        slidesPerView: 'auto',
        // spaceBetween: 40,
        loop: false,
    
        // Navigation arrows
        navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
    
        // And if we need scrollbar
        scrollbar: {
        el: '.swiper-scrollbar',
        },
    });

    // Swiper swiperUpute
    const swiperUpute = new Swiper('.swiperUpute', {
        // Optional parameters
        autoHeight: true,
        direction: 'horizontal',
        slidesPerView: 'auto',
        loop: false,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    // Check if homepage
    // if(document.querySelector('body').classList.contains('home')) {

    // Pitanja harmonika
    var pitanjeOtvori = document.querySelectorAll('.pitanje__button');
    pitanjeOtvori.forEach(button => {
        button.addEventListener('click', function(evt) {
            if(this.classList.contains('pitanje__button__add')){
                this.classList.toggle('is-hidden');
                this.nextElementSibling.classList.toggle('is-hidden');
            } else {
                this.classList.toggle('is-hidden');
                this.previousElementSibling.classList.toggle('is-hidden');
            }
            button.parentElement.nextElementSibling.classList.toggle('is-hidden');
        })
    })

    document.querySelector('.pitanje__button__add').classList.toggle('is-hidden');
    document.querySelector('.pitanje__button__add').parentElement.nextElementSibling.classList.toggle('is-hidden');
    document.querySelector('.pitanje__button__remove').classList.toggle('is-hidden');

    // Uputstva tabs
    var uputstvoSadrzaji = document.querySelectorAll('.uputstva__sadrzaj');
    var uputstvoSadrzajiMob = document.querySelectorAll('.uputstva__tekst--mob');
    var uputstvoNaslovi = document.querySelectorAll('.uputstva__naslov');
    var brojPrikazClanka = 1;
    var brojSlijClanka;
    var prikazaniClanak = document.querySelector('.uputstva__sadrzaj__' + brojPrikazClanka);
    var slijedeciClanak;
    var underscoreDesktop = document.querySelectorAll('.uputstva__naslov__aktivan');

    // Uputstva tabs / Initial display
    uputstvoSadrzaji.forEach(clanak => clanak.style.display = 'none');
    uputstvoSadrzaji[0].style.display = 'flex';
    if (viewWidth < 1025) { 
        document.querySelector('.uputstva__naslov__' + brojPrikazClanka).style.borderBottom = '4px solid #ff4d5a';
    }

    if (viewWidth > 767) {
        uputstvoNaslovi.forEach(uputstvo => {
            uputstvo.addEventListener('click', prikaziSadrzaj)
        })
    }

    // Upustva tabs / Show-hide underscore on window resize
    var resizeTimer;

    window.addEventListener('resize', function() {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
        
            viewWidth = document.querySelector('body').offsetWidth;

            if (viewWidth < 1025) {
                underscoreDesktop.forEach(underscore => underscore.style.display = 'none')
                document.querySelector('.uputstva__naslov__' + brojPrikazClanka).style.borderBottom = '4px solid #ff4d5a';
            } else {
                document.querySelector('.uputstva__naslov__' + brojPrikazClanka).style.borderBottom = 'none';
                document.querySelector('.uputstva__naslov__aktivan__' + brojPrikazClanka).style.display = 'block';
            }
                
        }, 250);

    });

    function prikaziSadrzaj(evt) {
        brojSlijClanka = this.classList.value.charAt(this.classList.value.length - 1);
        slijedeciClanak = document.querySelector('.uputstva__sadrzaj__' + brojSlijClanka);
        uputstvoSadrzaji.forEach(clanak => clanak.style.display = 'none'); //
        slijedeciClanak.style.display = 'flex';
        document.querySelector('.uputstva__naslov__aktivan__' + brojPrikazClanka).style.display = 'none';
        document.querySelector('.uputstva__naslov__aktivan__' + brojSlijClanka).style.display = 'block';
        // Tablet underscore active
        if (viewWidth < 1025) { 
            document.querySelector('.uputstva__naslov__' + brojPrikazClanka).style.borderBottom = 'none';
            this.style.borderBottom = '4px solid #ff4d5a';
        }
        brojPrikazClanka = brojSlijClanka;
    }

    // Uputstva / dodaj linkove u sadržaj
    var urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi; //
    var sadrzaji = [];

    for (let index = 0; index < uputstvoSadrzaji.length; index++) {
        sadrzaji[index] = uputstvoSadrzaji[index].firstElementChild.nextElementSibling.innerText;
        sadrzaji[index] = linkify(sadrzaji[index]);
        uputstvoSadrzaji[index].firstElementChild.nextElementSibling.innerHTML = `<p>${sadrzaji[index]}</p>`;
        uputstvoSadrzajiMob[index].innerHTML = `${sadrzaji[index]}`;
    }

    function linkify(text) {
        return text.replace(RegExp(urlRegex, "g"), function(adresa) {
            return `<a href="http://${adresa}" target="_blank" style="color: #ff4d5a">${adresa}</a>`;
        });
    }

    // Dokumenti prikaži sve
    var dokumentiSvi = document.querySelectorAll('.tease-dokument');
    var dokumentiAktiv = document.querySelectorAll('.dokument__aktivnost');
    var dokumDugmePlus = document.querySelector('.dokumenti__dugme--vise');
    var dokumDugmeMinus = document.querySelector('.dokumenti__dugme--manje');
    var brojOtvorenihDok;

    if (viewWidth < 800) { brojOtvorenihDok = 3 } else { brojOtvorenihDok = 6 }

    dokumentiSvi.forEach(dokument => dokument.style.display = 'none');
    dokumentiAktiv.forEach(aktivnost => {
        if (aktivnost.innerText == "Активно") {
            aktivnost.style.color = '#ff4d5a';
            aktivnost.style.opacity = 1;
        }
    });

    for (let index = 0; index < brojOtvorenihDok; index++) {
        dokumentiSvi[index].style.display = 'flex';
    }

    dokumDugmePlus.addEventListener('click', prikaziDokumente);
    dokumDugmeMinus.addEventListener('click', sakrijDokumente);

    function prikaziDokumente() {
        for (let index = brojOtvorenihDok; index < dokumentiSvi.length; index++) {
            dokumentiSvi[index].style.display = 'flex';
        }
        zamjenaDugmadi(dokumDugmePlus, dokumDugmeMinus);
    }
    function sakrijDokumente() {
        for (let index = brojOtvorenihDok; index < dokumentiSvi.length; index++) {
            dokumentiSvi[index].style.display = 'none';
        }
        zamjenaDugmadi(dokumDugmePlus, dokumDugmeMinus);
    }

    function zamjenaDugmadi(dugme1, dugme2) {
        dugme1.classList.toggle('is-hidden');
        dugme2.classList.toggle('is-hidden');
    }

    // Pitanja prikaži sve
    var pitanjaSva = document.querySelectorAll('.pitanje');
    var pitanjaDugmePlus = document.querySelector('.pitanja__dugme--vise');
    var pitanjaDugmeMinus = document.querySelector('.pitanja__dugme--manje');

    pitanjaSva.forEach(dokument => dokument.style.display = 'none');

    for (let index = 0; index < 3; index++) {
        pitanjaSva[index].style.display = 'block';
    }

    pitanjaDugmePlus.addEventListener('click', prikaziPitanja);
    pitanjaDugmeMinus.addEventListener('click', sakrijPitanja);

    function prikaziPitanja() {
        for (let index = 3; index < pitanjaSva.length; index++) {
            pitanjaSva[index].style.display = 'block';
        }
        zamjenaDugmadi(pitanjaDugmePlus, pitanjaDugmeMinus);
    }

    function sakrijPitanja() {
        for (let index = 3; index < pitanjaSva.length; index++) {
            pitanjaSva[index].style.display = 'none';
        }
        zamjenaDugmadi(pitanjaDugmePlus, pitanjaDugmeMinus);
    }

} // End home check

// Galerija custom pagination numbers
    // Check if single page
    if(document.querySelector('body').classList.contains('single-post')) {

        // Swiper swiperGalerija
        const swiperGalerija = new Swiper('.swiperGalerija', {
            // Optional parameters
            spaceBetween: 0,
            allowTouchMove: true,
            // autoHeight: true,
            direction: 'horizontal',
            slidesPerView: 1,
            loop: false,

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            pagination: {
                el: '.swiper-pagination-bullets',
                type: 'bullets',
                clickable: true,
            },
        });

        var galerPagNum = document.querySelector('.vijesti__swiper-pagination--fraction');
        var sviBulleti = document.querySelectorAll('.swiper-pagination-bullet');
        var paginWrapp = document.querySelector('.swiper-pagination-bullets');
        var maksBrSlika = sviBulleti.length;
        var brojPrikSlike = 1;

        if (document.querySelector('.swiperGalerija')) { updatePagin(); }

        function updatePagin() {
            brojPrikSlike = swiperGalerija.realIndex + 1;
            galerPagNum.innerText = `${brojPrikSlike}\/${maksBrSlika}`;
        }

        swiperGalerija.on('slideChange', function () { updatePagin(); });

    }



