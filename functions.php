<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

/**
 * If you are installing Timber as a Composer dependency in your theme, you'll need this block
 * to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
 * plug-in, you can safely delete this block.
 */
$composer_autoload = __DIR__ . '/vendor/autoload.php';
if ( file_exists( $composer_autoload ) ) {
	require_once $composer_autoload;
	$timber = new Timber\Timber();
}

/**
 * This ensures that Timber is loaded and available as a PHP class.
 * If not, it gives an error message to help direct developers on where to activate
 */
if ( ! class_exists( 'Timber' ) ) {

	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function( $template ) {
			return get_stylesheet_directory() . '/static/no-timber.html';
		}
	);
	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
	/** Add timber support. */
	// add_action( 'wp_footer', 'footer_scripts' );
	// public function footer_scripts() {
    //     wp_enqueue_script('pageSpecificFootJs', get_template_directory_uri() . '/dist/js/page.specific.foot-dist.js', false, null);
    // }
	
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_clening' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'youtube_add_shortcode' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'amzwp_loadScripts' ) );
		parent::__construct();
	}

	/** This is where you can register custom post types. */
	public function register_post_types() {

	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['foo']   = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::context();';
		$context['menu']  = new Timber\Menu();
		$context['site']  = $this;
		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );

		
	}

	/** 
	 * Removing unwonted WP parts
	 */
	public function theme_clening() {
		/* Disable WordPress Admin Bar for all users */
		add_filter( 'show_admin_bar', '__return_false' );

		// Disable global RSS, RDF & Atom feeds.
		add_action( 'do_feed',      'disable_feeds', -1 );
		add_action( 'do_feed_rdf',  'disable_feeds', -1 );
		add_action( 'do_feed_rss',  'disable_feeds', -1 );
		add_action( 'do_feed_rss2', 'disable_feeds', -1 );
		add_action( 'do_feed_atom', 'disable_feeds', -1 );
	
		// Disable comment feeds.
		add_action( 'do_feed_rss2_comments', 'disable_feeds', -1 );
		add_action( 'do_feed_atom_comments', 'disable_feeds', -1 );
	
		// Prevent feed links from being inserted in the <head> of the page.
		add_action( 'feed_links_show_posts_feed',    '__return_false', -1 );
		add_action( 'feed_links_show_comments_feed', '__return_false', -1 );
		remove_action( 'wp_head', 'feed_links',       2 );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
	
		/* Remove Gutenberg Block Library CSS from loading on the frontend */
		function remove_wp_block_library_css(){
			wp_dequeue_style( 'wp-block-library' );
			wp_dequeue_style( 'wp-block-library-theme' );
			wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
		} 
		add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css', 100 );
	
		/* remove rsd_link */
		remove_action('wp_head', 'rsd_link');
	}

	// Should be called from within an init action hook
	function youtube_add_shortcode() { 
		add_shortcode( 'youtube', 'youtube_shortcode' );
	}

	/** 
	 * YouTube shortcode
	 */
	function youtube_shortcode( $atts ) {
		if( isset( $atts['id'] ) ) {
			$id = sanitize_text_field( $atts['id'] );
		} else {
			$id = false;
		}

		// This time we use Timber::compile since shortcodes should return the code
		return Timber::compile( 'youtube-short.twig', array( 'id' => $id ) );
	}



	/** This Would return 'foo bar!'.
	 *
	 * @param string $text being 'foo', then returned 'foo bar!'.
	 */
	public function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );
		$twig->addFilter( new Twig\TwigFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}

	/** Enqueue scripts.
	 *
	 * 	
	 */
	function amzwp_loadScripts() {
		$version = wp_get_theme()->get('Version');

		if (get_bloginfo('url') == 'http://budiefiskalizovan.gov.rs') {
			// wp_enqueue_script( 'amzw-foot-javascript', get_template_directory_uri() . '/dist/js/page.specific.head-dist.js', array('jquery'), $version, false );
			wp_enqueue_script( 'amzw-foot-javascript', get_template_directory_uri() . '/dist/js/page.specific.foot-dist.js', array('jquery'), $version, true );
		} else {
			wp_enqueue_script( 'amzw-head-javascript', get_template_directory_uri() . '/assets/script/head/page.specific.head.js', array('jquery'), $version, false );
			wp_enqueue_script( 'amzw-foot-javascript', get_template_directory_uri() . '/assets/script/foot/page.specific.foot.js', array('jquery'), $version, true );
		}
	}

}

// add_action( 'init', 'add_shortcode' );
// add_shortcode( 'youtube', 'youtube_shortcode' );

// function youtube_add_shortcode() { 
// 	add_shortcode( 'youtube', 'youtube_shortcode' );
// }

new StarterSite();
