## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is simple Lorem ipsum dolor generator.
	
## Technologies
Project is created with:

* Wordpress version: 5.7.2 (hr)
* Timber version: 1.0
* jQuery library version: 3.6.0
* bulma version: 0.9.2
	
## Setup
To setup dependencies
```
$ npm install
```